FROM maven:3.8.4-openjdk-11

MAINTAINER Georges KEMAYO

RUN mkdir -p /usr/share/app/ 
COPY ./target/meteo-*-SNAPSHOT.jar /usr/share/app/meteo.jar

CMD ["-jar", "/usr/share/app/meteo.jar"]
ENTRYPOINT ["java"]
