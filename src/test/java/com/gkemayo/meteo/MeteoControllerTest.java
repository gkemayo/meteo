package com.gkemayo.meteo;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest
public class MeteoControllerTest {

	@MockBean
	HttpServletRequest httpServletRequest;
	
	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	@Spy
	private MeteoController meteoController;

	@Test
	public void testGetWeatherByCity_with_IOException() throws Exception {
		
		mockMvc.perform(post("/meteo"))
		       .andExpect(view().name("meteo-page"))
		       .andExpect(content().string(containsString("Méteo de la ville de :")));
	}

}
