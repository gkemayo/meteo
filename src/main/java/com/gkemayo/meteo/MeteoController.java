package com.gkemayo.meteo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/meteo")
@SessionAttributes("data")
public class MeteoController {
	
	@Autowired
	@Qualifier("httpServletRequest")
	private HttpServletRequest httpServletRequest;
		
	@Value("${rapidapi.url}")
	private String rapidApiUrl;
	
	@Value("${rapidapi.key}")
	private String rapidApiKey;
	
	@Value("${rapidapi.host}")
	private String rapidApiHost;

	@PostMapping
	public String getWeatherByCity(Data data, Model model) {
		
		log.info("Call of api: " + httpServletRequest.getRequestURI() );
		
		HttpResponse<String> response;
		try {
			StringBuilder builder = new StringBuilder();
			builder.append(rapidApiUrl)
			       .append("?location=")
			       .append(data.getCity())
			       .append("&format=json")
			       .append("&u=c");
			
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(builder.toString()))
					.header("X-RapidAPI-Key", rapidApiKey)
					.header("X-RapidAPI-Host", rapidApiHost)
					.method("GET", HttpRequest.BodyPublishers.noBody())
					.build();
			
			response = callRapidApi(request);
			
			data.setResponse(response.body());
			model.addAttribute(data);
			
			log.info("api  " + httpServletRequest.getRequestURI() + " response with content: " + response.body());
			
		} catch (IOException | InterruptedException e) {
			log.error("An error occur with message : " + e);
		}		
		
		return "meteo-page";

	}

	protected HttpResponse<String> callRapidApi(HttpRequest request) throws IOException, InterruptedException {
		return HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
	}

}
