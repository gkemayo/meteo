package com.gkemayo.meteo;

import java.io.Serializable;

public class Data implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String city;

	private String response;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
